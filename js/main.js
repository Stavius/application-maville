//{3} Création d'une "class constructor" contenant les sous variables => name (nom de la ville), numberOfHabitants (nombres d'habitants), 
//localisation (code postal).
class Ville {
    constructor (name, numberOfHabitants, localisation) {
        this.name = name;
        this.numberOfHabitants = numberOfHabitants;
        this.localisation = localisation;
    }
};

//{4} Création d'une variable "city", pour générer un "local storage" (mesVilles)
var city = JSON.parse(localStorage.getItem('mesVilles'));

//{5}Ce "if" permet d'éviter les bugs si dans le "local storage" mesVille n'existe pas
if (city == null) {
    city = []
};

document.getElementById('btn_ok').addEventListener('click',btn_ok);

//Quand cette fonction quand elle est appelée :
function btn_ok(e) {
        e.preventDefault();//On rend le bouton "btn_ok" inopérant. (pour éviter les bugs)

    //On vérifie que tous les <input> de la page creation.html ont minimum un caractère
    if ((document.getElementById("inputa_city").value.length>0)
    && (document.getElementById("inputa_habitants").value.length>0) 
    && (document.getElementById("inputa_localisation").value.length>0)) {

        //On mes, ces élément dans la "classe constructor"
        city.push(new Ville(inputa_city.value, inputa_habitants.value, inputa_localisation.value));

        //On créer un message pour prévenir que la ville à bien été enregistré
        let monLi = document.createElement("li");
        monLi.innerHTML = "La ville de "+city[city.length-1].name 
        +" à bien été créé. Merci d'avoir participé.";
        document.getElementById("monLi").appendChild(monLi);

        //On enlève tous les caractères des <input> de la page creation.html
        document.getElementById("inputa_city").value = '';
        document.getElementById("inputa_habitants").value = '';
        document.getElementById("inputa_localisation").value = '';

        //On prend les éléments des <input> et on met dans le "Local storage"
        localStorage.setItem("mesVilles", JSON.stringify(city));
    //Sinon (si les <input> ne sont pas remplis) on envoie une "alert"
    } else {
        alert("Plusieurs champs n'on pas été remplis !");
    }
};

// Quand cette fonction quand elle est appelée :
function afficherTable(){

    //On met à jour le tableau de la page index.html
    table = document.getElementById('maTable');
    tr = document.createElement('tr');
    tr.innerHTML = "<td><h4>Nom</h4></td><td><h4>Nombres d'habitants</h4></td><td><h4>Code postal</h4></td><td><h4>Supprimer</h4></td><td><h4>Modifier</h4></td>";
    table.appendChild(tr);

    //La boucle "for" lui:
    for(i=0; i<city.length; i++) {
        
    //Récupère l'index des éléments dans le "Local storage"
        el=city[i];

        //Affiche le tableau de page index.html avec les éléments du "Local storage"
        tr = document.createElement('tr');  
        tr.innerHTML = '<td>' + el.name + '</td>'         
        + '<td>' + el.numberOfHabitants + '</td>' 
        + '<td>' + el.localisation + '</td>' 
        //{8} Bouton pour supprimer une ligne du "Local storage"
        + '<td>' + `<a href='#' onclick='btn_delete("${i}", this);'><img src='image/delete.png' alt='supprimer'></a>` + '</td>'
        //{11} Bouton pour modifier, il redirige vers la page change.html
        + '<td>' + "<a href='change.html?i="+i+"'><img src='image/edit.png' alt='modifier'></a>" + '</td>';
        table.appendChild(tr);
    }
};

//{9} Quand cette fonction quand elle est appelée :
function btn_delete(i, element) {

    //Créer une "alert" pour prévenir que l'élément dans le "Local storage" a bien été supprimer
    alert("La ville de "+city[i].name+" à bien été supprimé !");

    //On supprime l'élément du tableau dans le Local storage
    city.splice(i, 1);
    localStorage.setItem("mesVilles", JSON.stringify(city));

    //On met à jour la <table> de la page index.html
    table = document.getElementById('maTable');
    table.innerHTML = '';
    afficherTable();
};

// Quand cette fonction quand elle est appelée :
function btn_modif(i){

    //Elle afficher dans un <label> avec une petite phrase ainsi l'élément du tableau selectioner grâce à l'index, récupérer grâce a la boucle for
    document.getElementById("nameOfCity").innerHTML = "Le nom de la ville actuel est <strong>"+city[i].name+"</strong>";

    //Elle affiche ce même élément dans l'<input>
    document.getElementById("inputa_nameOfCity").value = city[i].name;

    document.getElementById("numberOfHabitants").innerHTML = "Le nombres d'habitants actuel est de <strong>"+city[i].numberOfHabitants+"</strong>";
    document.getElementById("inputa_numberOfHabitants").value = city[i].numberOfHabitants;

    document.getElementById("localisationOfCity").innerHTML = "Le code postal actuel est <strong>"+city[i].localisation+"</strong>";
    document.getElementById("inputa_localisationOfCity").value = city[i].localisation;
};

// Quand cette fonction quand elle est appelée :
function btn_modifOk(){

    //Même principe que pour le <if> précédent sauf que ces <input> sont sur la page change.html
    if ((document.getElementById("inputa_nameOfCity").value.length>0) 
    && (document.getElementById("inputa_numberOfHabitants").value.length>0) 
    && (document.getElementById("inputa_localisationOfCity").value.length>0)) {

        //On prend les éléments des <input>
        city[i].name = inputa_nameOfCity.value;
        city[i].numberOfHabitants = inputa_numberOfHabitants.value;
        city[i].localisation = inputa_localisationOfCity.value;

        //Et on les mets dans le "Local storage"
        localStorage.setItem("mesVilles", JSON.stringify(city));
    } else {
        alert("Plusieurs champs n'on pas été remplis !");
    }
};